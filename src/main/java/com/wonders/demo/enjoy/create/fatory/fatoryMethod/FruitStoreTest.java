package com.wonders.demo.enjoy.create.fatory.fatoryMethod;


import com.wonders.demo.enjoy.entity.Bag;
import com.wonders.demo.enjoy.entity.Fruit;

/**
 * 【工厂方法模式】
 * 水果店测试
 * Created by Peter on 10/8 008.
 */
public class FruitStoreTest {

    private static FruitFactory fruitFactory;
    private static BagFactory bagFactory;

    public static void main(String[] args) {
        fruitFactory = new AppleFactory();
//        fruitFactory = new BananaFactory();
//        fruitFactory = new OrangeFactory();
        Fruit fruit = fruitFactory.getFruit();
        fruit.draw();

//        bagFactory = new BananaBagFactory();
        bagFactory = new AppleBagFactory();
        Bag bag = bagFactory.getBag();

        bag.pack();
    }

}
