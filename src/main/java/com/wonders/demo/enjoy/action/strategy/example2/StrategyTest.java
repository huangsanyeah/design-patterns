package com.wonders.demo.enjoy.action.strategy.example2;

public class StrategyTest {
    public static void main(String[] args) {
        Trip trip;
        trip = new Trip(new Bike());
        trip.doTrip();
        trip = new Trip(new Drive());
        trip.doTrip();
        //====================================
        //优化后的写法 搭配工厂模式使用，直接传type即可选择不同的实现类
        Trip walk = new Trip("walk");
        walk.doTrip();
    }
}
