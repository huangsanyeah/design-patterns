package com.wonders.demo.enjoy.action.strategy.example3.spring.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * @author weiyue.huang
 */
@Getter
@AllArgsConstructor
public enum PayModeEnum {

    A_LI("alibaba", "阿里"),

    WE_CHAT("weChat", "微信"),


    BANK("bank", "银行");

    private String code;

    private String desc;


}
