package com.wonders.demo.enjoy.action.strategy.example3.spring.service;

import java.math.BigDecimal;

/**
 * @description 支付接口 - 策略接口
 */
public interface PayStrategy {

    /**
     * 支付方式
     */
    String getMode();

    /**
     * 支付接口
     */
    void pay(BigDecimal price);
}
