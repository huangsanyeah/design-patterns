package com.wonders.demo.enjoy.action.strategy.example3.spring.service.impl;

import com.wonders.demo.enjoy.action.strategy.example3.spring.constant.PayModeEnum;
import com.wonders.demo.enjoy.action.strategy.example3.spring.service.PayStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @author weiyue.huang
 * @desc 微信支付
 */
@Slf4j
@Component
public class WeChatPay implements PayStrategy {
    @Override
    public String getMode() {
        return PayModeEnum.WE_CHAT.getCode();
    }

    @Override
    public void pay(BigDecimal price) {
        log.info("一大堆业务代码.....");
        log.info("微信支付，支付了{}元", price);
    }
}
