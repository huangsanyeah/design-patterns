package com.wonders.demo.enjoy.action.template.example1;

public class TemplateTest {
    public static void main(String[] args) {
//        Refrigerator refrigerator = new Banana();
        Refrigerator refrigerator = new Apple();
        refrigerator.open();
        refrigerator.put();
        refrigerator.close();
    }
}
