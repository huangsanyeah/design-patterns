package com.wonders.demo.enjoy.action.strategy.example3.spring.controller;


import com.wonders.demo.enjoy.action.strategy.example3.spring.service.PayStrategyContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * 参考 https://github.com/kong0827/Design-Patterns
 */
@RestController
public class PayController {

    @Autowired
    PayStrategyContext context;

    @GetMapping
    public void test(@RequestParam(value = "mode") String mode) {
        context.useStrategy(mode, new BigDecimal(10));
    }
}
