package com.wonders.demo.enjoy.action.template.example1;

/**
 * 模板：模板方法模式是指定义一个模板结构（抽象类），将具体内容延迟到子类去实现
 * 优点：
 * 提高代码复用性：将相同部分的代码放在抽象的父类中，而将不同的代码放入不同的子类中；
 * 实现了反向控制：通过一个父类调用其子类的操作，通过对子类的具体实现扩展不同的行为，实现了反向控制并且符合开闭原则。
 * 以给冰箱中放水果为例，比如，我要放一个香蕉：开冰箱门 → 放香蕉 → 关冰箱门；如果我再要放一个苹果：开冰箱门 → 放苹果 → 关冰箱门。可以看出它们之间的行为模式都是一样的，只是存放的水果品类不同而已，这个时候就非常适用模板方法模式来解决这个问题，实现代码如下
 */
public abstract class Refrigerator {
    public void open() {
        System.out.println("开冰箱门");
    }

    public abstract void put();

    public void close() {
        System.out.println("关冰箱门");
    }
}
