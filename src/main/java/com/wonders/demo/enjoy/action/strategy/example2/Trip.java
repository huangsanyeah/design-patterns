package com.wonders.demo.enjoy.action.strategy.example2;

/**
 * 定义出行类
 */
public class Trip {
    private ITrip trip;

    public Trip(ITrip trip) {
        this.trip = trip;
    }

    public Trip(String type) {
        if (type.equals("drive")) {
            this.trip = new Drive();
        } else if (type.equals("bike")) {
            this.trip = new Bike();
        } else if (type.equals("walk")) {
            this.trip = new Walk();
        }
    }

    public void doTrip() {
        this.trip.going();
    }
}
