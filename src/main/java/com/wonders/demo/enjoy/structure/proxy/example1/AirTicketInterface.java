package com.wonders.demo.enjoy.structure.proxy.example1;

public interface AirTicketInterface {
    void buy();
}
