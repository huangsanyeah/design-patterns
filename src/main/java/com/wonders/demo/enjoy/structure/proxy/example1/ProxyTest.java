package com.wonders.demo.enjoy.structure.proxy.example1;

public class ProxyTest {
    public static void main(String[] args) {
        AirTicketInterface airTicket = new ProxyAirTicket();
        airTicket.buy();
    }
}
