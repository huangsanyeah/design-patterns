package com.wonders.demo.enjoy.structure.proxy.example1;

/**
 * 代理模式是给某一个对象提供一个代理，并由代理对象控制对原对象的引用。
 *
 * 优点：
 *
 * 代理模式能够协调调用者和被调用者，在一定程度上降低了系统的耦合度；
 * 可以灵活地隐藏被代理对象的部分功能和服务，也增加额外的功能和服务。
 * 缺点：
 * 由于使用了代理模式，因此程序的性能没有直接调用性能高；
 * 使用代理模式提高了代码的复杂度。
 * 举一个生活中的例子：比如买飞机票，由于离飞机场太远，直接去飞机场买票不太现实，这个时候我们就可以上携程 App 上购买飞机票，
 * 这个时候携程 App 就相当于是飞机票的代理商。
 */
public class AirTicket implements AirTicketInterface{
    @Override
    public void buy() {
        System.out.println("买票");
    }
}
