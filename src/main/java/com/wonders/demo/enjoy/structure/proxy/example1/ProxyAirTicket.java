package com.wonders.demo.enjoy.structure.proxy.example1;

/**
 * 代理类
 */
public class ProxyAirTicket implements AirTicketInterface {

    private AirTicket airTicket;

    public ProxyAirTicket() {
        airTicket = new AirTicket();
    }

    @Override
    public void buy() {
        airTicket.buy();
    }
}
